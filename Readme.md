## Demo strategies

#### Protocol
During the game your strategy has to answer two questions:
1. Which card to put (_ROUND_START_)
2. Which row to pick (_PICK_ROW_)

All communication goes via STDIN and STDOUT and single `\n` is required for proper command execution

#### Input
Your program gets single line JSON holding current `game state` and action name. `game state` looks same for both actions

_ROUND_START_
```
{ "action": "ROUND_START", "gameState": ...}
``` 

_PICK_ROW_
```
{ "action": "PICK_ROW", "gameState": ...}
``` 

#### Game State with comments
```
{
       "board": [
            [20, 25, 76, 83, 96],  // first row on table having 5 cards. If you put 97 - you'll grab the roww
            [15, 44], 
            [37, 40, 101], 
            [57, 60, 104]
        ],
       "hand": [8], // it's going to be last round and you have only one card left
       "playerPenalties": {
           "otherId": [28, 42, 81, 71, 69, 78, 68, 79, 17], // your opponent has these cards in his penalty stack
           "me": [],
       },
       "myId": "me"
   }
```

#### Strategy examples
Check out nearby folders for language you might know
