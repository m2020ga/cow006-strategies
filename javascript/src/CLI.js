const readline = require("readline")

function startStdIo(onPickRow, onRoundStart) {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    })

    function handleInput(input) {
        try {
            const inputParsed = JSON.parse(input)
            if (inputParsed.action === "PICK_ROW") {
                const action = onPickRow(inputParsed.gameState)
                console.log(action)
            } else if (inputParsed.action === "ROUND_START") {
                const action = onRoundStart(inputParsed.gameState)
                console.log(action)
            } else {
                throw new Error("Incorrect input given")
            }
        } catch (e) {
            console.error(e)
        }
    }
    rl.on('line', handleInput)
}

module.exports = {
    startStdIo,
}
