const {startStdIo} = require("./CLI")
const {onPickRow, onRoundStart} = require("./strategy")

startStdIo(onPickRow, onRoundStart)
