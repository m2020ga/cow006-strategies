const {onPickRow, onRoundStart} = require("../src/strategy")

const boardState = {
    "board": [[20, 25, 76, 83, 96], [15, 44], [37, 40, 101], [57, 60, 104]],
    "hand": [8],
    "playerPenalties": {
        "otherId": [28, 42, 81, 71, 69, 78, 68, 79, 17],
        "me": [],
    },
    "myId": "me"
}

describe("smoke tests", () => {
    test('ROUND_START', () => {
        expect(onRoundStart(boardState)).toEqual(8)
    })

    test('PICK_ROW', () => {
        expect([0, 1, 2, 3]).toContain(onPickRow(boardState))
    })
})


