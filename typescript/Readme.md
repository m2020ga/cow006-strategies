## Typescript strategy

#### What do I have?
Most simple strategy, that has no logic in it. Your aim is to improve it.

Also you have tests, that might help you.

#### What should I do?
1. `npm install`
2. Edit code in `src/MyStrategy.ts`
3. Run tests `npm run test`
4. Pack `npm run pack`
5. Upload `strategy.tar` to engine and give it a proper name
6. Return to step 2 and improve

#### NB!

* `package.json` should be included in TAR-file 
* `package.json` should contain script `run`, which will be launched during competition
