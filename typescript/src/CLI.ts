import { createInterface, Interface } from "readline";
import { GameState, Strategy } from "./interfaces";

export interface CLI_PickRow {
    action: "PICK_ROW"
    gameState: GameState
}

export interface CLI_RoundStart {
    action: "ROUND_START"
    gameState: GameState
}

export type CLI_Input = CLI_PickRow | CLI_RoundStart

export class CLI {
    private rl: Interface;

    constructor(private strategy: Strategy) {
        this.rl = createInterface({
            input: process.stdin,
            output: process.stdout
        })
        this.rl.on('line', this.handleInput);
    }

    private handleInput = (input: string) => {
        try {
            const inputParsed: CLI_Input = JSON.parse(input) as CLI_Input;
            if (inputParsed.action === "PICK_ROW") {
                const action = this.strategy.onPickRow(inputParsed.gameState);
                console.log(action);
            } else if (inputParsed.action === "ROUND_START") {
                const action = this.strategy.onRoundStart(inputParsed.gameState);
                console.log(action);
            } else {
                throw new Error("Action not specified")
            }
        } catch (e) {
            console.error(e);
        }
    }
}
