import { GameState, Strategy } from "./interfaces";

export class MyStrategy implements Strategy {
    constructor() {
    }

    public onPickRow(gameState: GameState): number{
        return 0;
    }

    public onRoundStart(gameState: GameState): number {
        return gameState.hand[0];
    }
}
