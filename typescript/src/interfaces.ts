export type Card = number;
export type Deck = Card[];

export interface GameState {
    hand: Deck
    playerPenalties: { [key: string]: Deck }
    board: [Deck, Deck, Deck, Deck]
    myId: string
}

export interface Strategy {
    onPickRow(gameState: GameState): number;
    onRoundStart(gameState: GameState): number;
}
