import { MyStrategy } from "../src/MyStrategy";
import { GameState } from "../src/interfaces";

const boardState: GameState = {
    "board": [[20, 25, 76, 83, 96], [15, 44], [37, 40, 101], [57, 60, 104]],
    "hand": [8],
    "playerPenalties": {
        "otherId": [28, 42, 81, 71, 69, 78, 68, 79, 17],
        "me": [],
    },
    "myId": "me"
}

describe("smoke tests", () => {
    test('ROUND_START', () => {
        const s = new MyStrategy()
        expect(s.onRoundStart(boardState)).toEqual(8)
    })

    test('PICK_ROW', () => {
        const s = new MyStrategy()
        expect([0, 1, 2, 3]).toContain(s.onPickRow(boardState))
    })
})
