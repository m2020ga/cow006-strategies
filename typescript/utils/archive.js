const spawn = require("child_process").spawn
const path = require("path")
var glob = require("glob")

archive()

function archive() {
    const srcPath = "./dist"
    const files = [
        ...glob.sync(path.join(srcPath, "**", "*.js"))
            .filter(f => /\.js$/.test(f)),
        "package.json"]

    console.log(files)
    const args = `cf strategy.tar --xform s%${srcPath}/%% ${files.join(" ")}`
    const process = spawn(`tar`, args.split(" "))
    process.stderr.on('data', err => console.log("[err]", err.toString()))
    process.once('error', err => console.error(err))
    console.log(`Built on ${(new Date()).toLocaleString()}`)
}

